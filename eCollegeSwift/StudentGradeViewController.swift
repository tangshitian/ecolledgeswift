//
//  StudentGradeViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 4/10/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class StudentGradeViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var ulCourseName: UILabel!
    
    @IBOutlet weak var ulGrade2DateLetter: UILabel!
    
    @IBOutlet weak var ulGrade2DateComments: UILabel!
    @IBOutlet weak var ulGrade2DateYourScore: UILabel!
    
    @IBOutlet weak var ulGrade2DateAvg: UILabel!
    @IBOutlet weak var tblItemlized: UITableView!
    
    var course:Course! = nil
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    let gc = GlobalConstants()
    var itemArray = [GradeBookItem]()
    var dateFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblItemlized.rowHeight = UITableViewAutomaticDimension
        self.tblItemlized.estimatedRowHeight = 110.0
        
        ulCourseName.text = course.getCourseTitle()
        
        //1. Get grade to date
        getGradeToDate()
        //2. Fill table with itemlized
        getItemlizedGrades()
        
    }
    
    func getItemlizedGrades() {
        let user = User(adl: appDel, userName: appDel.getUserName(), cliStr: gc.clientString())
        let path = "/users/\(user.getId())/courses/\(course.getCourseId())/gradebook/userGradebookItems?expand=grade"
        
        let json = doMethodEx(appDel, path, "GET", nil, nil, false)
        if json != nil {
            var dict = json["userGradebookItems"] as! NSArray
            for item in dict {
                //1. get id
                let gbId = item["id"] as! String
                //2. get gradebookitem
                let gbi = item["gradebookItem"] as! NSDictionary
                let type = gbi["type"] as! String
                let guid = gbi["id"] as! String
                let title = gbi["title"] as! String
                let pointsTotal = gbi["pointsPossible"] as! Float
                let weight = gbi["weightsPossible"] as! Float?
                let includedInGrade = gbi["isIncludedInGrade"] as! Bool
                let isExtraCredit = gbi["isExtraCredit"] as! Bool
                
                var lnks = item["links"] as! NSArray
                var _lnks = [Link]()
                for lnk in lnks {
                    var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: lnk["title"] as? String)
                    _lnks.append(_lnk)
                }
                var gradeItem = GradeItem(type: type, id: guid, title: title, pointsPossible: pointsTotal, weightsPossible: weight, includeInGrade: includedInGrade, isExtraCredit: isExtraCredit, links: _lnks)
                
                //3. get links
                let gblAry = item["links"] as! NSArray
                var grdLnks : [GradeLink] = [GradeLink]()
                for lnkitem in gblAry {
                    let grade = lnkitem["grade"] as! NSDictionary
                    //let id = grade["id"] as! Int
                    let points = grade["points"] as! Float
                    let letterGrade = grade["letterGrade"] as! String
                    let comments = grade["comments"] as! String
                    let share = grade["shareWithStudent"] as! Bool
                    let title = grade["title"] as! String
                    let updateDate = grade["updatedDate"] as! String
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                    var nsDate = dateFormatter.dateFromString(updateDate)
                    dateFormatter.dateFormat = "hh:mm yyyy-MM-dd"
                    
                    var ids = item["id"]
                    var idString : String! = "\(ids)"
                    idString = idString.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: idString.startIndex, end: idString.endIndex))
                    
                    var grdLnk = GradeLink(id: idString, points: points, letterGrade: letterGrade, comments: comments, shareWithStudent: share, updateDate:dateFormatter.stringFromDate(nsDate!) , title: title)
                    grdLnks.append(grdLnk)
                    
                }
                
                //4. create GradebookItem
                var grdbkitem = GradeBookItem(id: gbId, gradeItem: gradeItem, glink: grdLnks)
                
                //5. Add it to array
                itemArray.append(grdbkitem)
            }
            
        }
    }
    
    func getGradeToDate() {
        let user = User(adl: appDel, userName: appDel.getUserName(), cliStr: gc.clientString())
        
        let path = "/users/\(user.getId())/courses/\(course.getCourseId())/coursegradetodate"
        
        let json = doMethodEx(appDel, path, "GET", nil, nil, false)
        if json != nil {
            var cgt = json["courseGradeToDate"] as! NSDictionary
            let avg = cgt["average"] as! Int
            let earned = cgt["earned"] as! Int
            let total = cgt["possible"] as! Int
            var lg = cgt["letterGrade"] as! NSDictionary
            var lgGrade = lg["letterGrade"] as! String
            var lgComment = lg["comments"] as! String
            
            //TODO: remove this, just for demo
            if (lgGrade as NSString).length == 0 {
                lgGrade = "A-"
                lgComment = "For Demo"
            }
            ulGrade2DateLetter.text = lgGrade
            ulGrade2DateComments.text = lgComment
            ulGrade2DateYourScore.text = "Your score: \(earned) / \(total)"
            ulGrade2DateAvg.text = "Average: \(avg) / \(total)"
            
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = self.tblItemlized.dequeueReusableCellWithIdentifier("studenGradeTableCell") as! UITableViewCell
        let row = indexPath.row
        var item = itemArray[indexPath.row]
        
        //1. title
        let title = item.gradeItem.title
        let cellTitle = cell.viewWithTag(2) as! UILabel
        cellTitle.text = title
        
        //2. Post time
        if item.glink.count > 0 {
            let postTime = item.glink[0].updateDate
            let cellUpdate = cell.viewWithTag(3) as! UILabel
            cellUpdate.text = postTime
        
            //3. letter grade
            let lGrade = item.glink[0].letterGrade
            let cellLetter = cell.viewWithTag(4) as! UILabel
            //TODO: remove demo
            cellLetter.text = (lGrade as NSString).length > 0 ? lGrade : "C-"
        
            //4. comments
            let comments = item.glink[0].comments
            let cellComments = cell.viewWithTag(7) as! UILabel
            //TODO remove no comments
            cellComments.text = (comments as NSString).length > 0 ? comments : "No comments"
        
            //5. score
            let myScore = "\(item.glink[0].points) / \(item.gradeItem.pointsPossible)"
            let cellScore = cell.viewWithTag(5) as! UILabel
            cellScore.text = myScore
        }
        
        //6. include in grade or not
        let includeGrade = item.gradeItem.includeInGrade
        let cellInclude = cell.viewWithTag(6) as! UILabel
        cellInclude.textColor = includeGrade ? UIColor.blackColor() : UIColor.lightGrayColor()
        
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
    //    return 1
    //}
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setCourse(cur :Course) {
        self.course = cur
        
    }

}
