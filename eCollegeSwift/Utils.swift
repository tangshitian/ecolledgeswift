//
//  Utils.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 9/14/14.
//  Copyright (c) 2014 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

func httpRequestEx(urlPath: String, method: String!, headers: Dictionary<String, String>!, body:String!)-> (Int?, NSDictionary){
    var request = NSMutableURLRequest(URL: NSURL(string: urlPath)!)
    var session = NSURLSession.sharedSession()
    request.HTTPMethod = method
    println(body)
    if nil != body {
        request.HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding)
    }
    
    var response : NSHTTPURLResponse!
    
    //request.addValue("application/json", forHTTPHeaderField: "Accept")
    if (headers != nil) {
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        
    }
    //this is token request, need to be synchronized
    var err : NSError?
    var resp : NSURLResponse?
    var data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &resp, error:&err) as NSData!
    if data == nil {
        println("Get nil of the data, REST request failed")
        return (500, ["payload":0])
    }
    
    let retstr  = NSString(data: data, encoding: NSUTF8StringEncoding)
    if retstr != nil {
        println(retstr)
    }
    
    var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
    if json == nil {
        let cont = retstr as String!
        //TODO: support the case of non-html
        json = ["html":"\(cont)"]
    }
    let httpResp = resp as! NSHTTPURLResponse
    
    return (httpResp.statusCode, json!)
    
}

func refreshTokenRequest() -> Int{
    let gc = GlobalConstants()
    var urlPath: String = "\(gc.baseURL())/token"
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let reqBody = "client_id=\(gc.clientId())&grant_type=password&username=\(gc.clientString())\\\(appDelegate.getUserName())&password=\(appDelegate.getPasswd())"
    println(reqBody)
    let (ret, dict) = httpRequestEx(urlPath, "POST", nil, reqBody)
    println("refreshTokenRequest: return http status code =\(ret)")
    
    if dict["access_token"] != nil {
        var refresh_token = dict["refresh_token"] as? String
        var access_token = dict["access_token"] as? String
        var expires = dict["expires_in"] as? Int
        
        appDelegate.setAccessToken(access_token!)
    }
    return ret!
}


//Since header is only the fixed tokens, we ignore it for now
func createCachedRequestKey(reqUrl:String, method:String, body:String!) ->String {
    return body == nil ? "method=\(method) url=\(reqUrl) body=nobody" : "method=\(method) url=\(reqUrl) body=\(body)"
}

extension String {
    var html2String:String {
        return NSAttributedString(data: dataUsingEncoding(NSUTF8StringEncoding)!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:NSUTF8StringEncoding], documentAttributes: nil, error: nil)!.string
    }
}

func generateTimeStampId()->String {
    let dateFormat = NSDateFormatter()
    dateFormat.dateFormat = "yyyymmddhhmmssS"
    return dateFormat.stringFromDate(NSDate())
}

func htmlToPlain(htmlStr:String) ->String{
    return htmlStr.html2String
}

//Send REST Request with cache supported
func doMethodEx(appDel: AppDelegate, relatedPath:String, method:String, headersIn:Dictionary<String, String>!, body:String!, bforce : Bool!)->NSDictionary! {
    
    let gc = GlobalConstants()
    var reqUrl = relatedPath
    if reqUrl.rangeOfString("https") == nil {
        reqUrl = "\(gc.baseURL())/\(relatedPath)"
    }
    println("Url=\(reqUrl)")
    
    var cacheEnabled = appDel.cacheReqEnabled()
    var key :String? = nil
    if bforce == false {
        key = createCachedRequestKey(reqUrl, method, body)
        if cacheEnabled {
            var ret = appDel.getCachedRequest(key!)
            if ret != nil {
                return ret
            }
        }
    }
    
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    //not in cache, create new request and retrieve the result
    var headers = Dictionary<String, String>()
    if !appDel.getAccessToken().isEmpty {
        headers["X-Authorization"] = "Access_Token access_token=\(appDel.getAccessToken())"
    }
    if headersIn != nil {
        for key in headersIn.keys {
            headers[key] = headersIn[key]
        }
    }
    println(headers)
    
    var (code, dict) = httpRequestEx(reqUrl, method, headers, body)
    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    if 200 <= code && code < 400 {
        if cacheEnabled && key != nil {
            appDel.addCachedRequest(key!, value: dict)
        }
        return dict
    }
    if dict.count > 0 && dict["html"] != nil {
        let key = "html" //(dict["html"] != nil) ? "html" : "error"
        //show alert
        let codeVal = code
        var alert: UIAlertView = UIAlertView()
        alert.title = "Request Error"
        let sVal = dict[key] as! String
        alert.message = sVal.html2String
        alert.addButtonWithTitle(gc.textOk())
        var ubi = UIWebView()
        alert.addSubview(ubi)
        ubi.loadHTMLString(sVal, baseURL: nil)
        alert.show()
    }
    return nil
}


