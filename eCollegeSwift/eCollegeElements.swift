//
//  Course.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 9/17/14.
//  Copyright (c) 2014 Karvi Technologies, Inc. All rights reserved.
//

import Foundation

enum Role : String {
    case PROF = "Professor", TAST = "TeachingAssistant", STUD="Student"
    static let allValues = [PROF, TAST, STUD]
}

struct Link {
    var href:String?
    var rel:String?
    var title:String?
}

struct Member {
    //Member could be instructor, student
    var userId:String?
    var personalId:String?
    var firstName:String?
    var lastName:String?
    var email:String?
    var phone:String?
    var addr:String?
    //List of links
    var link:[Link]
}

struct CourseItem {
    var id: String
    var contentType: String
    var title: String
    var links: [Link]
}

struct ScheduleItem {
    var dueDate: String?
    var start: String
    var end:String
    var isInherited:Bool
    var canAccessBeforeStart:Bool
    var canAccessAfterEnd:Bool
}

struct ScheduleUnit {
    var id: String
    var title: String
    var schedule: ScheduleItem
    var links: [Link]
}

struct GradeItem {
    var type: String
    var id: String
    var title:String
    var pointsPossible: Float
    var weightsPossible: Float?
    var includeInGrade: Bool
    var isExtraCredit: Bool
    var links: [Link]
}

struct GradeLink {
    var id: String
    var points: Float
    var letterGrade:String
    var comments:String
    var shareWithStudent:Bool
    var updateDate:String
    var title:String
}

struct GradeBookItem {
    var id:String
    var gradeItem:GradeItem
    var glink:[GradeLink]
}

struct AnnouncementItem {
    var aid:String
    var subject:String
    var text:String
    var submitter:String
    var start:String
    var end:String
}

struct TopicItem {
    var id: String
    var title: String
    var description: String
    var links: [Link]
    init(tid:String, tpTitle:String, desc:String, lnks:[Link]) {
        self.id = tid
        self.title = tpTitle
        self.description = desc
        self.links = lnks
    }
}

struct ResponseItem {
    var id: String
    var title: String
    var description: String
    var date: String
    var author:[Link]
    var links:[Link]
}

struct ActivityItem {
    var title: String
    var postTime:String?
    var actor:String?
    var summary:String?
}

class User {
    var id: Int!
    var userName: String!
    var fName: String!
    var lName: String!
    var eMail:String!
    var cliString:String!
    var appDel:AppDelegate
    
    init(adl: AppDelegate, userId:String) {
        let rPath = "users/\(userId)"
        self.appDel = adl
        var json = doMethodEx(adl, rPath, "GET", nil, nil, false)
        let dict = json["users"] as! NSArray
        decodeDict(dict)
    }
    
    init (adl : AppDelegate, userName : String, cliStr:String) {
        self.appDel = adl
        let rPath = "users/loginid=\(cliStr)%7C\(userName)"   //DONOT use '|', NSURL will not let it pass
        var json = doMethodEx(adl, rPath, "GET", nil, nil, false)
        let dict = json["users"] as! NSArray
        decodeDict(dict)
    }
    
    func decodeDict(dict : NSArray) {
        if dict.count == 1 {
            self.id = dict[0]["id"] as! Int!
            self.userName = dict[0]["userName"] as! String!
            self.fName = dict[0]["firstName"] as! String!
            self.lName = dict[0]["lastName"] as! String!
            self.eMail = dict[0]["emailAddress"] as! String!
     //       self.cliString = cliStr
        }
    }
    
    func getUser()->(id:Int, userName:String, fName:String, lName:String, mail:String ) {
        return (self.id, self.userName, self.fName, self.lName, self.eMail)
    }
    
    func getId()->String! {
        return "\(self.id)"
    }
    
    /**
     * Check if this user is student of a course
     */
    func isStudentOfCourse(courseId:String)->Bool {
        
        let rPath = "users/\(getId())/courses/\(courseId)/role"
        var json = doMethodEx(self.appDel, rPath, "GET", nil, nil, false)
        if json != nil {
            let dict = json["role"] as! NSDictionary
            if dict["type"] as! String! == "STUD" {
                return true
            }
        }
        return false
    }
}

class Course {
    var id: String
    var displayCourseCode: String
    var intro_text: String?
    var intro_audio_lnk:String?
    var intro_video_lnk:String?
    var title: String
    var instructors :[Link]
    var tas : [Link]
    var students : [Link]
    var links : [Link]
    var callNumbers : [String]
    var appDel : AppDelegate
    
    init (adl : AppDelegate, courseId : String) {
        id = courseId
        appDel = adl
        
        //Retrieve content via the REST call
        var json = doMethodEx(appDel, "courses/\(id)", "GET", nil,nil, false)
        
        var dict : NSArray = json["courses"] as! NSArray
        //parse the value, shall be only one course
        displayCourseCode = dict[0]["displayCourseCode"] as! String
        
        title = dict[0]["title"] as! String
        
        //take out course introductions
        /*
        if let _intro = dict[0]["courseIntroduction"] {
            var intro = _intro as NSDictionary
            intro_text = intro["text"] as? String
            intro_audio_lnk = intro["audiolink"] as? String
            intro_video_lnk = intro["videolink"] as? String
        }*/
        
        //take out callNumbers
        var _callNumbers = dict[0]["callNumbers"] as! NSArray
        callNumbers = [String]()
        for it in _callNumbers {
            callNumbers.append(it as! String)
        }
        //init instructor
        
        instructors = [Link]()
        tas = [Link]()
        students = [Link]()
        links = [Link]()
        
        var keys = ["instructors", "teachingAssistants", "students"]
        for key in keys {
            var it_dict = dict[0][key] as! NSDictionary
            var toApp = instructors
            switch (key) {
            case "teachingAssistants":
                toApp = tas
                break
            case "students":
                toApp = students
                break
            case "links":
                toApp = links
                break
            default:
                toApp = instructors
            }
            var _links : NSArray = it_dict["links"] as! NSArray
            for link in _links {
                let tt = link["title"]
                var lnk = Link(href: link["href"] as? String,
                                rel: link["rel"] as? String,
                    title: (tt == nil) ? "" : tt as? String)
                    toApp.append(lnk)
                    
            }
            
        }
        
        //take out links
        var _links : NSArray = dict[0]["links"] as! NSArray
        for link in _links {
            var lnk = Link(href: link["href"] as? String,
                rel: link["rel"] as? String,
                title: "")
            links.append(lnk)
            
        }
        
    }
    
    func getCourseIntro_Text() ->String {
        return intro_text!
    }
    
    func getCourseIntro_Audio()->String {
        return intro_audio_lnk!
    }
    
    func getCourseIntro_Video()->String {
        return intro_video_lnk!
    }
    
    func getCourseCallNumbers()->[String] {
        return callNumbers
    }
    
    func getCourseId () ->String {
        return id
    }
    
    func getCourseTitle() ->String {
        return title
    }
    
    func getCourseDisplayCode() ->String {
        return displayCourseCode
    }
    
    func getCourseInstructors() ->[Link] {
        return instructors
    }
    
    func getTAs() ->[Link] {
        return tas
    }
    
    func getCourseStudents() ->[Link] {
        return students
    }
    
}


class Term {
    /*
        {\"terms\":[{\"description\":\"\",\"id\":71553,\"name\":\"Partner Development Term\",\"startDateTime\":\"2013-09-01T00:00:00Z\",\"endDateTime\":\"2030-08-01T00:00:00Z\"}]}"
     */
    var desp: String?
    var id: UInt?
    var name: String?
    var startDateTime: String?
    var endDateTime: String?
    
    init(id: UInt, name: String, desp:String, sDate:String, eDate:String) {
        self.id = id
        self.name = name
        self.desp = desp
        self.startDateTime = sDate
        self.endDateTime = eDate
    }
    
    func getTerm() -> (retId:UInt, retName:String, retDesp:String, retSdate:String, retEdate:String) {
        return (id!, name!, desp!, startDateTime!, endDateTime!)
    }
}

class DiscussItem {
    var id:String?
    var introText:String?
    var links:[Link]?
    var appDel : AppDelegate?
    init (adl : AppDelegate, discussId : String,  intro: String, lnks:[Link]) {
        id = discussId
        appDel = adl
        introText = intro
        links = lnks
    }
    
    func getId()->String! {
        return self.id
    }
    
    func getDicussItem()->(cId:String, intro : String, lnks:[Link]) {
        return (id!, introText!, links!)
    }
}