//
//  ClassViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 9/8/14.
//  Copyright (c) 2014 Karvi Technologies, Inc. All rights reserved.
//

/*
Course Info and Enrollments (Roster) - retrieving only
Content Structure and Course Content - retrieving only
Course Schedule - retrieving only
Dropbox (assignment submission) - retrieving structure & creating documents
Exams - supports taking exams outside LearningStudio (i.e. in a mobile app)
Grades - retrieving & creating
Threads - retrieving & creating
Activity Feed - retrieving only
Announcements - retrieving & creating (role restricted)
Document Sharing - retrieving & creating
Tracking - creating only
User Information - retrieving only
 */
import UIKit

class CoursesViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var coursesView: UITableView!
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    
    let gc = GlobalConstants()
    var courseArray = [Course]()
    /*
    var term:Term! = nil
    
    func setTerm(t: Term) {
        self.term = t
    }*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = gc.courseView_navBackground()
        //self.navigationController?.navigationBar.setBackgroundImage(gc.getImage_schoolLogo(), forBarMetrics: .Default)
        self.coursesView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.coursesView.dataSource = self
        self.coursesView.tableHeaderView = nil
        let path = "me/courses"
        //let path = "terms/\(term.getTerm().retId)/courses"
        println(path)
        var json = doMethodEx(appDel, path, "GET", nil, nil, false)
        var dict : NSArray = json["courses"] as! NSArray
        for item in dict {
            var links : NSArray = item["links"] as! NSArray
            for link in links {
                
                var courseLnk : String = link["href"] as! String
                var arr = split(courseLnk) {$0 == "/"}
                var cid = arr[arr.count - 1]
                var course = Course(adl: appDel, courseId: cid)
                
                courseArray.append(course)
            }
        }
    }
    
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        /*
        var course : Course = courseArray[indexPath.row]
        
        var alert: UIAlertView = UIAlertView()
        alert.title = course.getCourse().retTitle
        alert.message = course.getCourse().retUrl
        alert.addButtonWithTitle("Done")
        alert.show()
        */
    }
override     
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1 
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courseArray.count
    }
    
    func imageResize (imageObj:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        imageObj.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellId = "courseLstCell"
        var cell:UITableViewCell = self.coursesView.dequeueReusableCellWithIdentifier(cellId) as! UITableViewCell
        cell.textLabel?.font = UIFont(name: "Arial", size: 12.0)
        cell.textLabel?.text = courseArray[indexPath.row].getCourseTitle()
        //cell.textLabel?.text = courseArray[indexPath.row].getCourseDisplayCode()
        var courseImage : UIImage = UIImage(named: "Course.png")!
        imageResize(courseImage, sizeChange: CGSize(width: courseImage.size.width/2, height: courseImage.size.height/2))
        cell.imageView?.layer.cornerRadius = courseImage.size.width / 2
        cell.imageView?.clipsToBounds = true
        
        cell.imageView?.image = courseImage
        return cell
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var idx : Int! = coursesView.indexPathForSelectedRow()?.row
        let course = courseArray[idx]
        if idx != nil {
            let vc = segue.destinationViewController as! CourseViewController
            vc.setCurrentCourse(course)
        }
    }
    
}