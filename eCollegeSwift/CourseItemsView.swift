//
//  CourseItemsView.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 2/11/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class CourseItemsView: UITableViewController {
    let gc = GlobalConstants()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    var itemArray = [CourseItem]()
    
    @IBOutlet var itemsView: UITableView!
    var course:Course! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        var json = doMethodEx(appDel, "me/courses/\(course.getCourseId())/items", "GET", nil, nil, false)
        if json == nil {
            return
        }
        var dict : NSArray = json["items"] as! NSArray
        for item in dict {
            
            var lnks = item["links"] as! NSArray
            var _lnks = [Link]()
            for lnk in lnks {
                var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: lnk["title"] as? String)
                _lnks.append(_lnk)
            }
            
            var ids = item["id"]
            var idString : String! = "\(ids)"
            idString = idString.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: idString.startIndex, end: idString.endIndex))
            var it = CourseItem(id: idString,
                contentType: item["contentType"] as! String,
                title: item["title"] as! String,
                links: _lnks)
            if it.title.hasPrefix("Ass") || it.title.hasPrefix("Qui") {
                itemArray.append(it)
            }
        }
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        cell = self.itemsView.dequeueReusableCellWithIdentifier("crseItem") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "crseItem")
        }
        cell!.textLabel?.font = UIFont(name: "Arial", size: 16.0)
        let val = itemArray[indexPath.row].title as String!
         cell!.textLabel?.text = val
        
        var img : UIImage = UIImage(named: "ticon.png")!
        cell!.imageView?.layer.cornerRadius = img.size.width / 2
        cell!.imageView?.clipsToBounds = true
        
        cell!.imageView?.image = img
        
        return cell!
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let idxPath = self.itemsView.indexPathForSelectedRow()?.row
        if segue.destinationViewController is CourseItemViewController {
            let vc = segue.destinationViewController as! CourseItemViewController
            vc.setCourseItem(itemArray[idxPath!])
            vc.setCourse(self.course)
            return
        }
    }
    
}
