//
//  DiscussViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 1/12/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class DiscussViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate{
    let gc = GlobalConstants()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    var courseId:String! = nil
    var topic:TopicItem! = nil
    var contentItemId:String! = nil
    var dateFormatter = NSDateFormatter()
    
    var respArray = [ResponseItem]()
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var scrlView: UIScrollView!
    
    @IBOutlet weak var descView: UITextView!
    @IBOutlet weak var respTableView: UITableView!
    @IBOutlet weak var topicView: UIWebView!
    @IBOutlet weak var titleView: UITextField!
    @IBAction func addMedia(sender: AnyObject) {
        //attachment
    }
    
    @IBAction func butSend(sender: AnyObject) {
        //submit the discuss
        
        let title = titleView.text
        let desc = descView.text
        //Add post select, if no select, it is the top, if has anything selected, reply to that on
        let idxPath = self.respTableView.indexPathForSelectedRow()?.row
        var path = "courses/\(courseId)/threadeddiscussions/\(contentItemId)/topics/\(topic.id)/responses"
        if idxPath >= 0 {
            let postId =  respArray[idxPath!].id
            path = "\(path)/\(postId)/responses"
        }
        
        let body = "{\"response\":{\"title\":\"\(title)\",\"description\":\"\(desc)\"}}"
        var json = doMethodEx(appDel, path, "POST", ["Content-Type":"application/json"], body, false)
        if json != nil {
            titleView.text = ""
            descView.text = ""
            refreshResponse()
            
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let center = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo
        println("keyboard will show called")
        if let info = userInfo {
            let aniDurObject = info[UIKeyboardAnimationCurveUserInfoKey] as! NSValue
            let kbEndRetObj = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            
            var aniDur = 0.0
            var kbEndRect = CGRectZero
            aniDurObject.getValue(&aniDur)
            kbEndRetObj.getValue(&kbEndRect)
            
            let win = UIApplication.sharedApplication().keyWindow
            
            kbEndRect = view.convertRect(kbEndRect, fromView: win)
            
            let coveredRect = CGRectIntersection(view.frame, kbEndRect)
            
            UIView.animateWithDuration(aniDur, animations: {
                [weak self] in  //hope text field also scroll due to it is above
                self!.scrlView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: coveredRect.size.height * 5 / 7, right: 0)
                var pt = CGPoint(x: self!.scrlView.frame.minX, y: self!.scrlView.frame.minY + coveredRect.size.height * 5 / 7)
                self!.scrlView.setContentOffset(pt, animated: true)
                
            })
            
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let userInfo = notification.userInfo 
        if let info = userInfo {
            let aniDurObject = info[UIKeyboardAnimationCurveUserInfoKey] as! NSValue
            let kbEndRetObj = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            
            var aniDur = 0.0
            var kbEndRect = CGRectZero
            aniDurObject.getValue(&aniDur)
            kbEndRetObj.getValue(&kbEndRect)
            
            let win = UIApplication.sharedApplication().keyWindow
            
            kbEndRect = view.convertRect(kbEndRect, fromView: win)
            
            let coveredRect = CGRectIntersection(view.frame, kbEndRect)
            
            UIView.animateWithDuration(aniDur, animations: {
                [weak self] in  //hope text field also available
                self!.scrlView.contentInset = UIEdgeInsetsZero
            })
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navItem.title = htmlToPlain(topic.title)
        topicView.loadHTMLString(topic.description, baseURL: nil)
        self.respTableView.rowHeight = UITableViewAutomaticDimension
        self.respTableView.estimatedRowHeight = 128.0
        refreshResponse()
        
    }
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // delete annoucement
            //TODO: prompt for confirmation
            let postId = respArray[indexPath.row].id
            let rPath = "courses/\(courseId)/threadeddiscussions/\(contentItemId)/topics/\(topic.id)/responses/\(postId)"
            var json = doMethodEx(appDel, rPath, "DELETE", nil, nil, true)
            refreshResponse()
        }
    }
    
    func textFieldShouldReturn(textField : UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    
    func refreshResponse() {
        respArray.removeAll(keepCapacity: false)
        var json = doMethodEx(appDel, "courses/\(courseId)/threadeddiscussions/\(contentItemId)/topics/\(topic.id)/responses", "GET", nil, nil, true)
        if json != nil {
            var dict : NSArray = json["responses"] as! NSArray
            for item in dict {
                //var pid = item["id"] as! Int
                var title = item["title"] as! String
                var desc = item["description"] as! String
                var time = item["postedDate"] as! String
                
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                let postDate = dateFormatter.dateFromString(time)
                dateFormatter.dateFormat = "HH:mm:ss@MMM dd"
                
                var lnks = item["links"] as! NSArray
                var _lnks = [Link]()
                for lnk in lnks {
                    var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: lnk["title"] as? String)
                    _lnks.append(_lnk)
                }
                
                var _authlnks = [Link]()
                let auth = item["author"] as! NSDictionary
                var alnks = auth["links"] as! NSArray
                for lnk in alnks {
                    var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: lnk["title"] as? String)
                    _authlnks.append(_lnk)
                }
                var ids = item["id"]
                var idString : String! = "\(ids)"
                idString = idString.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: idString.startIndex, end: idString.endIndex))
                
                var respItem = ResponseItem(id: idString, title: title, description: desc, date: dateFormatter.stringFromDate(postDate!), author: _authlnks, links: _lnks)
                
                respArray.append(respItem)
                
            }
        }
        self.respTableView.reloadData()
        if respArray.count > 0 {
            
            let delay = 0.1 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            
            dispatch_after(time, dispatch_get_main_queue(), {
                self.respTableView.scrollToRowAtIndexPath(NSIndexPath(forRow: self.respArray.count - 1, inSection: 0), atScrollPosition: .Bottom, animated: true)
            })
            
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return respArray.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func getPostAuth(postId: String) ->(fName:String, lName:String) {
        var json = doMethodEx(appDel, "courses/\(courseId)/threadeddiscussions/\(contentItemId)/topics/\(topic.id)/responseAndAuthorComps/\(postId)", "GET", nil, nil, false)
        if json == nil {
            //?
            return ("", "")
        }
        let dict = json["responseAndAuthorComps"] as! NSArray
        for item in dict {
            let auth = item["authorRosterMember"] as! Dictionary<String, AnyObject>!
            if auth != nil {
                let fName = auth["firstName"] as! String!
                let lName = auth["lastName"] as! String!
                return (fName, lName)
            }
        }
        
        return ("", "")
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println ("didSelectRow :\(indexPath.row)")
        let str = htmlToPlain(respArray[indexPath.row].title)
        titleView.text = "Re: \(str)"
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.respTableView.dequeueReusableCellWithIdentifier("discussCell") as! UITableViewCell
        
        let val = respArray[indexPath.row].title as String!
        let titleLB:UILabel = cell.viewWithTag(5) as! UILabel
        titleLB.text = htmlToPlain(val)
        
        //Author, do it later
        /*
        let authLnks = respArray[indexPath.row].author
        var auth :User? = nil
        for lnk in authLnks {
            let str = (lnk.href)!
            if str.rangeOfString("users") != nil {
                var lst = split(str){$0=="/"}
                auth = User(adl: appDel, userId: lst[lst.count - 1])
                break
            }
        }*/
        let (fName, lName) = getPostAuth(respArray[indexPath.row].id)
        let name:UILabel = cell.viewWithTag(2) as! UILabel
        name.text = "\(fName) \(lName)"
        
        let dt = respArray[indexPath.row].date as String!
        let tm:UILabel = cell.viewWithTag(3) as! UILabel
        tm.text = dt
        
        let desc = respArray[indexPath.row].description as String!
        let detail:UILabel = cell.viewWithTag(4) as! UILabel
        detail.text = desc
        
        var img : UIImage = UIImage(named: "mb.png")!
        let icon:UIImageView = cell.viewWithTag(7) as! UIImageView
        icon.image = img
        
        return cell
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setInputIds( cid:String, tpc:TopicItem, did:String) {
        self.courseId = cid
        self.topic = tpc
        self.contentItemId = did
    }
    
    
}
