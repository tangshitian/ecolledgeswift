//
//  AppDelegate.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 8/10/14.
//  Copyright (c) 2014 Karvi Technologies, Inc. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?
    var token_expire: Int?
    var token_refresh: String?
    var token_access: String?
    var userName: String?
    var password: String?
    var cacheRestRequest: Bool?
    
    //this is for request cache for performance
    var requestCache = [String:NSDictionary]()
    
    //var managedObjectContext: NSManagedObjectContext?
    
    func setExpire(expire : Int) {
        self.token_expire = expire
    }
    
    /**
     Check if cached request enabled
     */
    func cacheReqEnabled()->Bool {
        //hardcoded it for now, put it in settings later
        return true //cacheRestRequest!
    }
    
    func clearCachedRequest() {
        requestCache.removeAll(keepCapacity: false)
    }
    
    func removeCachedRequest(var key: String) {
        requestCache.removeValueForKey(key)
    }
    
    func getCachedRequest(key:String)->NSDictionary! {
        return requestCache[key]
    }
    
    //Insert new value into the cache
    func addCachedRequest(key:String, value:NSDictionary) {
        requestCache[key] = value
    }
    
    //Update the enable cached request or not
    func enableRequestCache(val:Bool) {
        cacheRestRequest = val
    }
    
    func getExpire() ->Int {
        return self.token_expire!
    }
    
    func setAccessToken(acc : String) {
        self.token_access = acc
    }
    
    func getAccessToken() ->String {
        return self.token_access!
    }
    
    func applyUserName(str : String) {
        self.userName = str
    }
    
    func getUserName() ->String {
        return self.userName!
    }
    
    func setPasswd(str : String) {
        self.password = str
    }
    
    func getPasswd() ->String {
        return self.password!
    }
    
    /*
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: NSDictionary?) -> Bool {
       
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        // Override point for customization after application launch.
        self.window!.backgroundColor = UIColor.whiteColor()
        self.window!.makeKeyAndVisible()
        
        cacheRestRequest = true
        return true
    }*/

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //var managed

}

