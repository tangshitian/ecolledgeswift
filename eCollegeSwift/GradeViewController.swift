//
//  GradeViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 2/11/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class GradeViewController: UIViewController {
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    
    let gc = GlobalConstants()
    
    @IBOutlet weak var cName: UILabel!
    
    @IBOutlet weak var gradeResult: UITextView!
    @IBOutlet weak var itemView: UITableView!
    var course:Course! = nil
    var grdArray = [GradeItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cName.text = course.getCourseTitle()
        
        var json = doMethodEx(appDel, "courses/\(course.getCourseId())/gradebookItems", "GET", nil, nil, false)
        if json == nil {
            return
        }
        var dict : NSArray = json["gradebookItems"] as! NSArray
        for item in dict {
            var lnks = item["links"] as! NSArray
            var _lnks = [Link]()
            for lnk in lnks {
                var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: "")
                _lnks.append(_lnk)
            }
            
            var it = GradeItem(type: item["type"] as! String,
                id: item["id"] as! String,
                title: item["title"] as! String,
                pointsPossible: item["pointsPossible"] as! Float,
                weightsPossible: item["weightsPossible"] as? Float,
                includeInGrade: item["isIncludedInGrade"] as! Bool,
                isExtraCredit: item["isExtraCredit"] as! Bool,
                links: _lnks)
             grdArray.append(it)
        }
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var grd = grdArray[indexPath.row]
        //display the score at bottom, render it later
        gradeResult.text = "You got :\(grd.pointsPossible)"
        /*
        var course : Course = courseArray[indexPath.row]
        
        var alert: UIAlertView = UIAlertView()
        alert.title = course.getCourse().retTitle
        alert.message = course.getCourse().retUrl
        alert.addButtonWithTitle("Done")
        alert.show()
        */
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return grdArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        cell = self.itemView.dequeueReusableCellWithIdentifier("gradeCell") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "gradeCell")
        }
        cell!.textLabel?.font = UIFont(name: "Arial", size: 16.0)
        var grd = grdArray[indexPath.row]
        let title = grd.title as String!
        cell!.textLabel?.text = "\(title)    (total:\(grd.pointsPossible))"
        
        var img : UIImage = UIImage(named: "pen.png")!
        cell!.imageView?.layer.cornerRadius = img.size.width / 2
        cell!.imageView?.clipsToBounds = true
        
        cell!.imageView?.image = img
        
        return cell!
    }
    

}
