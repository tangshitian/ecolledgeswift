//
//  CourseViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 1/10/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class CourseViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var courseTable: UITableView!
    @IBOutlet weak var courseTitle: UILabel!
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var teachGrade: UITableViewCell!
    @IBOutlet weak var StudentGrades: UITableViewCell!
    @IBOutlet weak var navItem: UINavigationItem!
    let gc = GlobalConstants()
    
    var ciArray = [CourseItem]()
    
    let itemArray = ["Syllabus", "Activity feed", "Course Info", "Anouncements", "Group", "Grades"]
    var course:Course! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.courseTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.courseTable.dataSource = self
        
        var json = doMethodEx(appDel, "me/courses/\(course.getCourseId())/items", "GET", nil, nil, false)
        if json == nil {
            return
        }
        var dict : NSArray = json["items"] as! NSArray
        for item in dict {
            
            var lnks = item["links"] as! NSArray
            var _lnks = [Link]()
            for lnk in lnks {
                var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: lnk["title"] as? String)
                _lnks.append(_lnk)
            }
            var ids = item["id"]
            var idString : String! = "\(ids)"
            idString = idString.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: idString.startIndex, end: idString.endIndex))
            
            var it = CourseItem(id: idString,
                contentType: item["contentType"] as! String,
                title: item["title"] as! String,
                links: _lnks)
            ciArray.append(it)
        }
        
        
        //Decide if we need display gradeItem for teacher or student
        let user = User(adl: appDel, userName: appDel.getUserName(), cliStr: gc.clientString())
        if !user.isStudentOfCourse(course.getCourseId()) {
            StudentGrades.hidden = true
            teachGrade.hidden = true //TODO: enable this
            /*
            let idx = NSIndexPath(forRow: 4, inSection: 1)
            courseTable.beginUpdates()
            courseTable.deleteRowsAtIndexPaths([idx!], withRowAnimation: UITableViewRowAnimation.Automatic)
            courseTable.endUpdates()
            */
        } else {
            //hide student item
            teachGrade.hidden = true
        }
        
    }
    
    func setCurrentCourse(courseTobe:Course) {
        
        course = courseTobe
        
        if let s = course.getCourseTitle() as String? {
            navItem.title = "\(s)(\(course.getCourseId()))"
        } else {
            navItem.title = "(\(course.getCourseId()))"
        }
    }
    
    func findContentItemId( name:String, type:String) ->[String] {
        var lst = [String]()
        for it in ciArray { 
            if it.title == name && type == it.contentType {
                lst.append(it.id)
            }
        }
        return lst
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController is SyllabusViewController {
            let vc = segue.destinationViewController as! SyllabusViewController
            vc.setCourse(course)
            return
        }
        if segue.destinationViewController is AnnounceViewController {
            let vc = segue.destinationViewController as! AnnounceViewController
            vc.setCourse(course)
            return
        }
        if segue.destinationViewController is DiscussListViewController {
            let vc = segue.destinationViewController as! DiscussListViewController
            vc.setCourse(course)
            
            let discussIds = findContentItemId("Discussion", type: "THREAD")
            vc.setContentId(discussIds)
            
            return
        }
        if segue.destinationViewController is CourseItemsView {
            let vc = segue.destinationViewController as! CourseItemsView
            vc.setCourse(course)
            return
        }
        
        if segue.destinationViewController is ScheduleViewController {
            let vc = segue.destinationViewController as! ScheduleViewController
            vc.setCourse(course)
            return
        }
        
        if segue.destinationViewController is StudentGradeViewController {
            let vc = segue.destinationViewController as! StudentGradeViewController
            vc.setCourse(course)
            return
        }
        
        if segue.destinationViewController is GradeViewController {
            let vc = segue.destinationViewController as! GradeViewController
            vc.setCourse(course)
            return
        }
        
        if segue.destinationViewController is ActFeedViewController {
            let vc = segue.destinationViewController as! ActFeedViewController
            vc.setCourse(course)
            return
        }
        
        var tabBarController = segue.destinationViewController as! UITabBarController;
        //var tabDestinationViewController: AnyObject? = tabBarController.viewControllers?.first
        var cnt = tabBarController.viewControllers?.count
        for var i = 0; i < cnt; i++ {
            var vcd: AnyObject? = tabBarController.viewControllers?[i]
            if vcd is InstructorsView {
                let vc = vcd as! InstructorsView
                vc.setCourse(course)
            }
            
            if vcd is StudentsViewController {
                let vc = vcd as! StudentsViewController
                vc.setCourse(course)
            }
            
            if vcd is TAsViewController {
                let vc = vcd as! TAsViewController
                vc.setCourse(course)
            }
            
            if vcd is DroppedViewController {
                let vc = vcd as! DroppedViewController
                vc.setCourse(course)
            }
        }
        
        
        
    }
    
    
    #if TEST
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.courseTable.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        cell.textLabel?.text = itemArray[indexPath.row]
        /*
        switch(indexPath.row) {
        case 0: //Syllabus load syllabus icon
            break
        case 1: //
            break
        case 2: //
            break
        default:
            break
        }
        */
        var itemImage : UIImage = UIImage(named: "ticon.png")!
        cell.imageView?.layer.cornerRadius = itemImage.size.width / 2
        cell.imageView?.clipsToBounds = true
        cell.imageView?.image = itemImage
        
        return cell
    }
    #endif
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func shouldPerformSegueWithIdentifier(identifier: String!, sender: AnyObject!) -> Bool {
        if identifier != nil && identifier == "grade2Student" {
            let user = User(adl: appDel, userName: appDel.getUserName(), cliStr: gc.clientString())
            if !user.isStudentOfCourse(course.getCourseId()) {
                let alert = UIAlertView()
                alert.title = "Do not go"
                alert.message = "You are not a student!"
                alert.addButtonWithTitle("Ok")
                alert.show()
                
                return false
            }
        }
        
        // by default, transition
        return true
    }
}
