//
//  LoginViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 8/10/14.
//  Copyright (c) 2014 Karvi Technologies, Inc. All rights reserved.
//

import UIKit
import CoreData

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var txtUserName:UITextField!
    @IBOutlet var txtPasswd:UITextField!
    @IBOutlet var btnRemember:UISwitch!
    @IBOutlet var info:UILabel!
    @IBOutlet var login:UIButton!
    @IBOutlet var forgotpsw:UIButton!
    
    let gc = GlobalConstants()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    /*
    var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
    var path = paths.stringByAppendingPathComponent("ec.plist")!
    var fileManager = NSFileManager.defaultManager()
    if (!(fileManager.fileExistsAtPath(path)))
    {
        var bundle : NSString = NSBundle.mainBundle().pathForResource("Config", ofType: "plist")
        fileManager.copyItemAtPath(bundle, toPath: path, error:nil)
    }
    let dictCfg = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("Config", ofType: "plist")!)
    */
    var dataGot = NSMutableArray()
    var loginSuccess = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtUserName.text = gc.userNameStudent()
        txtPasswd.text = "letmein"
        if btnRemember.enabled {
            //var usrName = dictCfg.objectForKey("USERNAME") as String
            //txtUserName.text = usrName
            
        } else {
            txtUserName.text = ""
        }
        
        login.backgroundColor = gc.loginButton().bkColor
        txtUserName.delegate = self
        txtPasswd.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveUserName() {
        if !txtUserName.text.isEmpty { 
            //dictCfg.setValue(txtUserName.text, forKey: "USERNAME")
        }
    }
    
    func textFieldShouldReturn(textField : UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func loginAccount() {
        saveUserName()
        
        appDel.applyUserName(txtUserName.text)
        appDel.setPasswd(txtPasswd.text)
        if (200 != refreshTokenRequest()) {
            txtUserName.text = ""
            txtPasswd.text = ""
            var alert: UIAlertView = UIAlertView()
            alert.title = gc.login_alertTitle()
            alert.message = gc.login_alertMsg()
            alert.addButtonWithTitle(gc.textOk())
            alert.show()
        } else {
            loginSuccess = true
        }
    }
    
    //Avoid next view if login failed
    override func shouldPerformSegueWithIdentifier(identifier: String!, sender: AnyObject!) -> Bool {
        return loginSuccess
    }
    
    @IBAction func forgotPassword() {
        var alert = UIAlertController(title: gc.passwordReset_Title(),
            message: gc.passwordReset_Msg(),
            preferredStyle: UIAlertControllerStyle.Alert)
        //TODO: set text align left
        alert.addTextFieldWithConfigurationHandler({
            (txtField: UITextField!) in txtField.placeholder = self.gc.textEmailAddress(); txtField.keyboardType
                = UIKeyboardType.EmailAddress})
        alert.addAction(UIAlertAction(title: gc.textCancel(), style: UIAlertActionStyle.Default, handler: nil))
        alert.addAction(UIAlertAction(title: gc.textContinue(), style: .Default, handler: { action in
            let email : String = ((alert.textFields![0] as! UITextField).text)
            //println("Continue with email =\(email)")
            //send password reset email
            let path = "/authorize/password/\(email)"
            
            println(path)
            var json = doMethodEx(self.appDel, path, "GET", nil, nil, false)
            //check the response
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    /*
    // #pragma mark - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
