//
//  ScheduleViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 2/11/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI

class ScheduleViewController: UIViewController {
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    
    let gc = GlobalConstants()
    
    @IBOutlet weak var cName: UILabel!
    
    @IBOutlet weak var schTblView: UITableView!
    var course:Course! = nil
    var schArray = [ScheduleUnit]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cName.text = course.getCourseTitle()
        refreshSchedule()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        cell = self.schTblView.dequeueReusableCellWithIdentifier("schCell") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "schCell")
        }
        cell!.textLabel?.font = UIFont(name: "Arial", size: 16.0)
        let title = schArray[indexPath.row].title as String!
        let s_tart = schArray[indexPath.row].schedule.start as String!
        let s_end = schArray[indexPath.row].schedule.end as String!
        
        let name:UILabel = cell?.viewWithTag(1) as! UILabel
        name.text = title
        
        let lbStart:UILabel = cell?.viewWithTag(2) as! UILabel
        lbStart.text = s_tart
        
        let lbEnd:UILabel = cell?.viewWithTag(3) as! UILabel
        lbEnd.text = s_end
        
        var img : UIImage = UIImage(named: "pen.png")!
        let icon:UIImageView = cell?.viewWithTag(4) as! UIImageView
        icon.image = img
        
        return cell!
    }
    
    func refreshSchedule() {
        var json = doMethodEx(appDel, "courses/\(course.getCourseId())/itemschedules", "GET", nil, nil, false)
        if json == nil {
            return
        }
        var dict : NSArray = json["itemSchedules"] as! NSArray
        for item in dict {
            var lnks = item["links"] as! NSArray
            var _lnks = [Link]()
            for lnk in lnks {
                var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: "")
                _lnks.append(_lnk)
            }
            var id = item["itemId"] as! Int
            var title = item["itemTitle"] as! String
            
            //parse schedule item
            var schJson = item["schedule"] as! NSDictionary
            var dueDate = schJson["dueDate"] as? String
            var accSch = schJson["accessSchedule"] as! NSDictionary
            
            var start = accSch["startDateTime"] as? String
            var end = accSch["endDateTime"] as? String
            var isInherited = accSch["isInherited"] as? Bool
            var canAccBfStart = accSch["canAccessBeforeStartDateTime"] as? Bool
            var canAccAfEnd = accSch["canAccessAfterEndDateTime"] as? Bool
            //TODO: add to calendar or expand to calendar
            //addEvent(title, start: start!, end: end!, cal: EKCalendar(), evtStore: EKEventStore(), notes: "")
            schArray.append(ScheduleUnit(
                id: "\(id)",
                title: title,
                schedule:ScheduleItem(
                    dueDate: dueDate,
                    start: start!, end: end!, isInherited: isInherited!, canAccessBeforeStart: canAccBfStart!, canAccessAfterEnd: canAccAfEnd!),
                links: _lnks
                ))
        }
    }
    
    
    func addEvent(title:String, start:String, end:String, cal:EKCalendar, evtStore:EKEventStore, notes:String) -> Bool {
        //2013-09-01T06:00:00Z
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        start.substringToIndex(start.endIndex.predecessor()) //Remove last Z
        let nsStart : NSDate = dateFormatter.dateFromString(start)!
        end.substringToIndex(end.endIndex.predecessor())
        let nsEnd : NSDate = dateFormatter.dateFromString(end)!
        
        if cal.allowsContentModifications == false {
            println("Too bad, calendar is readonly")
            return false
        }
        
        var event = EKEvent(eventStore: evtStore)
        event.calendar = cal
        event.title = title
        event.notes = notes
        event.startDate = nsStart
        event.endDate = nsEnd
        var error:NSError?
        return evtStore.saveEvent(event, span: EKSpanThisEvent, commit: true, error: &error)
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
