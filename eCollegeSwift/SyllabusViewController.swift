//
//  SyllabusViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 1/13/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit
import CoreData

class SyllabusViewController: UIViewController {
    
    @IBOutlet weak var sylbView: UIWebView!
    @IBOutlet weak var navItem: UINavigationItem!
    
    let gc = GlobalConstants()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    var course:Course! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        let sample = "<html><head></head><body><div><b>Syllabus</b></div><BR><BR><p>This is an example syllabus of \(course.getCourseTitle())</p></body></html>"
        
        var json = doMethodEx(appDel,
            "courses/\(course.getCourseId())/syllabus",
            "GET", nil, nil, false)
        if let htm = json as NSDictionary? {
            sylbView.loadHTMLString(htm["html"] as! String, baseURL: nil)
        } else {
            sylbView.loadHTMLString(sample, baseURL: nil)
        }
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }

}
