//
//  ActFeedViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 1/12/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class ActFeedViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource{
    let gc = GlobalConstants()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    var course:Course! = nil
    var actArray = [ActivityItem]()
    var dateFormatter = NSDateFormatter()
    @IBOutlet var actTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.actTable.rowHeight = UITableViewAutomaticDimension
        self.actTable.estimatedRowHeight = 60.0
        //Retrieve last activity
        let user = User(adl: appDel, userName: appDel.getUserName(), cliStr: gc.clientString())
        
        var path = "users/\(user.getId())/lastactivityfeed?courseid=\(course.getCourseId())"
        var json = doMethodEx(appDel, path, "GET", nil, nil, false)
        if json != nil {
            let dict = json["activityStream"] as! NSDictionary
            let title = dict["title"] as! String
            let itemAry = dict["items"] as! NSArray
            var postTime : String? = nil
            var name : String? = nil
            var summary : String? = nil
            for item in itemAry {
                if item["postedTime"] != nil {
                    //reformat
                    let pt = item["postedTime"] as! String
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ";
                    let dateGot = dateFormatter.dateFromString(pt)
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    postTime = dateFormatter.stringFromDate(dateGot!)
                    
                }
                
                if item["object"] != nil {
                    let obj = item["object"] as! NSDictionary
                    summary = obj["summary"] as! String!
                }
                
                if item["actor"] != nil {
                    let actor = item["actor"] as! NSDictionary
                    name = actor["title"] as! String!
                }
                
            }
            
            var actItem = ActivityItem(title: title, postTime: postTime, actor: name, summary: summary)
            
            actArray.append(actItem)
        }
        
    }
    
    override
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actArray.count
    }
    
    override
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = self.actTable.dequeueReusableCellWithIdentifier("actCell") as! UITableViewCell
        let row = indexPath.row
        
        var item = actArray[indexPath.row]
        let titleView = cell.viewWithTag(3) as! UILabel
        titleView.text = item.title
        
        let actorView = cell.viewWithTag(4) as! UILabel
        actorView.text = ""
        if item.actor != nil {
            actorView.text = item.actor
        }
        
        let timeView = cell.viewWithTag(5) as! UILabel
        timeView.text = ""
        if item.postTime != nil {
            timeView.text = item.postTime
        }
        
        let summayView = cell.viewWithTag(6) as! UILabel
        summayView.text = ""
        if item.summary != nil {
            summayView.text = item.summary
        }
        
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
    
}
