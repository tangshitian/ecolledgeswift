//
//  AnnounceViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 1/12/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class AnnounceViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate {
    
    let gc = GlobalConstants()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    var course:Course! = nil
    var itemArray = [AnnouncementItem]()
    var dateFormatter = NSDateFormatter()
    @IBOutlet weak var navItem: UINavigationItem!
    
    @IBOutlet weak var subjectView: UITextField!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var scrlMe: UIScrollView!
    @IBAction func submitAnnounce(sender: AnyObject) {
        //Submit a new annoucement
        let subject = subjectView.text
        let detail = detailView.text //attributedText
        let dateNow = NSDate()
        let submit = User(adl: appDel, userName: appDel.getUserName(), cliStr: gc.clientString())
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let start = dateFormatter.stringFromDate(dateNow)
        let end = dateFormatter.stringFromDate(dateNow.dateByAddingTimeInterval(864000)) //10days
        let body = "{\"submission\":{\"announcement\":{\"subject\":\"\(subject)\",\"text\":\"\(detail)\",\"startDisplayDate\":\"\(start)Z\",\"endDisplayDate\":\"\(end)Z\"},\"submitter\":{\"id\":\"\(submit.getId())\"}}}"
        let json = doMethodEx(appDel, "courses/\(course.getCourseId())/announcements/submission", "POST", ["Content-Type":"application/json"], body, false)
        refreshAnnouces()
        subjectView.text = ""
        detailView.text = ""
    }
    
    @IBOutlet weak var detailView: UITextView!
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let center = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo
        println("keyboard will show called")
        if let info = userInfo {
            let aniDurObject = info[UIKeyboardAnimationCurveUserInfoKey] as! NSValue
            let kbEndRetObj = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            
            var aniDur = 0.0
            var kbEndRect = CGRectZero
            aniDurObject.getValue(&aniDur)
            kbEndRetObj.getValue(&kbEndRect)
            
            let win = UIApplication.sharedApplication().keyWindow
            
            kbEndRect = view.convertRect(kbEndRect, fromView: win)
            
            let coveredRect = CGRectIntersection(view.frame, kbEndRect)
            
            UIView.animateWithDuration(aniDur, animations: {
                [weak self] in  //hope text field also scroll due to it is above
                
                self!.scrlMe.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: coveredRect.size.height * 3 / 4, right: 0)
                var pt = CGPoint(x: self!.subjectView.frame.minX, y: self!.scrlMe.frame.minY + coveredRect.size.height * 3 / 4)
                self!.scrlMe.setContentOffset(pt, animated: true)

            })
            
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let userInfo = notification.userInfo
        if let info = userInfo {
            let aniDurObject = info[UIKeyboardAnimationCurveUserInfoKey] as! NSValue
            let kbEndRetObj = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            
            var aniDur = 0.0
            var kbEndRect = CGRectZero
            aniDurObject.getValue(&aniDur)
            kbEndRetObj.getValue(&kbEndRect)
            
            let win = UIApplication.sharedApplication().keyWindow
            
            kbEndRect = view.convertRect(kbEndRect, fromView: win)
            
            let coveredRect = CGRectIntersection(view.frame, kbEndRect)
            
            UIView.animateWithDuration(aniDur, animations: {
                [weak self] in  //hope text field also available
                self!.scrlMe.contentInset = UIEdgeInsetsZero
            })
            
        }
    }
    
    
    func textFieldShouldReturn(textField : UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 60.0
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        refreshAnnouces()
        
    }
    
    func refreshAnnouces() {
        itemArray.removeAll(keepCapacity: false)
        let path = "courses/\(course.getCourseId())/announcements"
        var json = doMethodEx(appDel, path, "GET", nil, nil, true)
        if json != nil {
            var dict : NSArray = json["announcements"] as! NSArray
            let dateNow = NSDate()
            
            
            for item in dict {
                let start = item["startDisplayDate"] as! String
                let end = item["endDisplayDate"] as! String
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                let startDate = dateFormatter.dateFromString(start)
                dateFormatter.dateFormat = "HH:mm:ss@MMM dd"
                /*
                //skip those display date expired
                if  dateNow.compare(dateFormatter.dateFromString(end)!) == NSComparisonResult.OrderedDescending {
                continue
                }*/
                var ids = item["id"]
                var idString : String! = "\(ids)"
                idString = idString.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: idString.startIndex, end: idString.endIndex))
                
                var it = AnnouncementItem(aid: idString,
                    subject: item["subject"] as! String,
                    text: item["text"] as! String,
                    submitter: item["submitter"] as! String,
                    start: dateFormatter.stringFromDate(startDate!),
                    end: item["endDisplayDate"] as! String)
                //start, end)
                itemArray.append(it)
            }
            
        }
        self.tblView.reloadData()
        if itemArray.count > 0 {
            let delay = 0.1 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            
            dispatch_after(time, dispatch_get_main_queue(), {
                self.tblView.scrollToRowAtIndexPath(NSIndexPath(forRow: self.itemArray.count - 1, inSection: 0), atScrollPosition: .Bottom, animated: true)
            })
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // delete annoucement
            //TODO: prompt for confirmation
            let aId = itemArray[indexPath.row].aid
            let rPath = "courses/\(course.getCourseId())/announcements/\(aId)/submission"
            var json = doMethodEx(appDel, rPath, "DELETE", nil, nil, true)
            refreshAnnouces()
            self.tblView.reloadData()
        }
    }
        
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("annCell") as? UITableViewCell
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = self.tblView.dequeueReusableCellWithIdentifier("annCell") as! UITableViewCell
        let row = indexPath.row
        var item:AnnouncementItem = itemArray[indexPath.row]
        let readFlag = cell.viewWithTag(2) as! UIImageView
        readFlag.image = UIImage(named: "mb.png")
        
        let subjectView = cell.viewWithTag(3) as! UILabel
        subjectView.text = item.subject
        
        let authView = cell.viewWithTag(4) as! UILabel
        authView.text = item.submitter
        
        let tmView = cell.viewWithTag(5) as! UILabel
        tmView.text = item.start
        
        let contentView = cell.viewWithTag(6) as! UILabel
        contentView.text = item.text
        
        return cell
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
}
