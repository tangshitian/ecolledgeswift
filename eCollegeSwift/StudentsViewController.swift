//
//  StudentsViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 2/10/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    @IBOutlet weak var cName: UILabel!
    @IBOutlet weak var tblView: UITableView!
    let gc = GlobalConstants()
    var memArray = [Member]()
    
    var course:Course! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        cName.text = "Name: \(course.getCourseTitle())"
        if (course == nil) {return}
        var json = doMethodEx(appDel, "courses/\(course.getCourseId())/students", "GET", nil, nil, false)
        if (json == nil) {
            return
        }
        var dict : NSArray = json["students"] as! NSArray
        for item in dict {
            var lnks = item["links"] as! NSArray
            var _lnks = [Link]()
            for lnk in lnks {
                var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: "")
                _lnks.append(_lnk)
            }
            //var id = item["id"] as! Int
            var ids = item["id"]
            var idString : String! = "\(ids)"
            idString = idString.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: idString.startIndex, end: idString.endIndex))
            var it = Member(userId: idString,
                personalId: "", firstName: item["firstName"] as? String,
                lastName: item["lastName"] as? String,
                email: item["emailAddress"] as? String, phone: "", addr: "", link: _lnks)
            memArray.append(it)
        }
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        cell = self.tblView.dequeueReusableCellWithIdentifier("studentCell") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "studentCell")
        }
        cell!.textLabel?.font = UIFont(name: "Arial", size: 16.0)
        let fname = memArray[indexPath.row].firstName as String!
        let lname = memArray[indexPath.row].lastName as String!
        let email = memArray[indexPath.row].email as String!
        let name:UILabel = cell?.viewWithTag(2) as! UILabel
        name.text = "\(fname) \(lname) "
        
        let mail:UILabel = cell?.viewWithTag(3) as! UILabel
        mail.text = "\(email)"
        
        var img : UIImage = UIImage(named: "person.png")!
        let icon:UIImageView = cell?.viewWithTag(4) as! UIImageView
        icon.image = img
        
        return cell!
    }
}
