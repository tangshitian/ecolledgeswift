//
//  CourseItemViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 3/5/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

//Notes:
/**
1. Take out Assignment.
    - call: https://api.learningstudio.com/courses/11267415/items/100637797520
     got link 3 href:
       https://api.learningstudio.com/courses/11267415/textMultimedias/100637797520
    -call link 3 href got:
    {
        textMultimedias: [1]
        0:  {
            id: 100637797520
        contentUrl: "https://api.learningstudio.com/courses/11267415/textMultimedias/100637797520/content.html"
    }-
    -
    }
   - call content url

2. Take out Quiz
   -call: https://api.learningstudio.com/courses/11267415/items/100637797546
   got link 3 href: https://api.learningstudio.com/courses/11267415/exams/72943697
   -call: https://api.learningstudio.com/courses/11267415/exams/72943697
*/

import UIKit

class CourseItemViewController: UIViewController {
    
    @IBOutlet weak var navItem: UINavigationItem!
    
    let gc = GlobalConstants()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var webHeaderView: UIWebView!
    @IBOutlet weak var webView: UIWebView!
    var courseItem : CourseItem! = nil
    var course : Course! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let htmlTxt = "<html><body><h1><table><tr><td><b>Motto - </b></td><td>Study is my life!</td></tr></h1></body></html>"
        
        webHeaderView.loadHTMLString(htmlTxt, baseURL: nil)

        //Take out the first link
        var url = courseItem.links[0].href!
        url = url.stringByReplacingOccurrencesOfString("/access", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        
        var json = doMethodEx(appDel, url, "GET", nil, nil, false)
        if self.courseItem.title.rangeOfString("Assignment", options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
            //This is assignment
            var contentUrl : String? = nil
            if json != nil {
                var dict : NSArray = json["items"] as! NSArray
                for item in dict {
                    var ids = item["id"]
                    var idString : String! = "\(ids)"
                    idString = idString.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: idString.startIndex, end: idString.endIndex))
                    let idStr : String = idString
                    var lnks = item["links"] as! NSArray
                    for lnk in lnks {
                        let urlHref = lnk["href"] as! NSString
                        var range = urlHref.rangeOfString(idStr)
                        if range.location == urlHref.length - (idStr as NSString).length {
                            contentUrl = urlHref as String
                            break
                        }
                    }
                    if contentUrl != nil {
                        break
                    }
                }
            }
            if contentUrl != nil {
                var json2 = doMethodEx(appDel, contentUrl!, "GET", nil, nil, false)
                
                if json2 != nil {
                    var dict = json2["textMultimedias"] as! NSArray
                    for item in dict {
                        let s = item["contentUrl"] as! String
                        contentUrl = s
                    }
                    var json3 = doMethodEx(appDel, contentUrl!, "GET", nil, nil, false)
                    if json3 != nil {
                        let s = json3["html"] as! String!
                        webView.loadHTMLString(s, baseURL: nil)
                    }
                }
            }
            
        } else if self.courseItem.title.rangeOfString("Quiz", options: NSStringCompareOptions.CaseInsensitiveSearch) != nil{
            var contentUrl : String? = nil
            if json != nil {
                var dict : NSArray = json["items"] as! NSArray
                for item in dict {
                    let id = item["id"] as! Int
                    let idStr : String = "\(id)"
                    var lnks = item["links"] as! NSArray
                    for lnk in lnks {
                        
                        let urlRel = lnk["rel"] as! String!
                        if urlRel == "related" {
                            continue
                        }
                        let urlHref = lnk["href"] as! NSString
                        contentUrl = urlHref as String
                        
                    }
                    if contentUrl != nil {
                        break
                    }
                }
            }
            if contentUrl != nil {
                contentUrl = contentUrl!.stringByReplacingOccurrencesOfString("/courses", withString: "/me/courses", options: NSStringCompareOptions.LiteralSearch, range: nil)
                contentUrl = contentUrl!.stringByReplacingOccurrencesOfString("/exams", withString: "/examDetails", options: NSStringCompareOptions.LiteralSearch, range: nil)
                var json2 = doMethodEx(appDel, contentUrl!, "GET", nil, nil, false)
                if json2 != nil {
                    var dict : NSDictionary = json2["examDetail"] as! NSDictionary
                    var title = dict["title"] as! String!
                    if (title as NSString).length < 1 {
                        title = "Time for brainstorm"
                    }
                    //let desp = htmlToPlain(dict["description"] String!)
                    let maxTry = dict["maxAttempts"] as! Int
                    let dur = dict["duration"] as! Int
                    let gradebookReview = dict["gradebookReviewDate"] as! String!
                    
                    let htmlTxt = "<html><body><table>" + (((title as NSString).length < 1) ? "" : "<tr><td><b>Title</b></td><td>\(title)</td></tr><tr>") + "<td><b>maxAttempts</b></td><td>\(maxTry) times</td></tr><tr><td><b>Duration</b></td><td>\(dur) minutes</td></tr><tr><td><b>Gradebook Review Date</b></td><td>\(gradebookReview)</td></tr></table></body></html>"
                    
                    webHeaderView.loadHTMLString(htmlTxt, baseURL: nil)
                    webView.loadHTMLString(dict["description"] as! String!, baseURL: nil)
                    
                }
                
            }
            
            
        }
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCourseItem(cur :CourseItem) {
        self.courseItem = cur
        navItem.title = cur.title
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
    
}
