//
//  DiscussListViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 3/2/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class DiscussListViewController: UIViewController {
    let gc = GlobalConstants()
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    var course:Course! = nil
    var contentItemIds:[String]? = nil
    var itemArray = [TopicItem]()
    
    @IBOutlet weak var txtTopic: UITextField!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var contentView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (course == nil) {
            return
        }
        for cntId in contentItemIds! {
            var json = doMethodEx(appDel, "courses/\(course.getCourseId())/threadeddiscussions/\(cntId)/topics", "GET", nil, nil, false)
            if (json == nil) {
                return
            }
            var dict : NSArray = json["topics"] as! NSArray
            for item in dict {
                var lnks = item["links"] as! NSArray
                var _lnks = [Link]()
                for lnk in lnks {
                    var _lnk = Link(href: lnk["href"] as? String, rel: lnk["rel"] as? String, title: "")
                    _lnks.append(_lnk)
                }
                var title = ""
                if item["title"] != nil {
                    title = item["title"] as! String!
                }
                var desc = ""
                if item["description"] != nil {
                    desc = item["description"] as! String!
                }
                
                var ids = item["id"]
                var idString : String! = "\(ids)"
                idString = idString.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: idString.startIndex, end: idString.endIndex))
                
                var it = TopicItem(
                    tid: idString,
                    tpTitle: title, desc: desc, lnks: _lnks)
            
                itemArray.append(it)
            }
        }
        
    }
    
    @IBAction func addNewPost(sender: AnyObject) {
        /*
        let title = txtTopic.text
        let desp = contentView.attributedText
        let tpicId = generateTimeStampId()
        
        let body = "{\"response\":{\"title\":\"\(title)\",\"description\":\"\(desp)\"}}"
        var json = doMethodEx(appDel, "/courses/\(course.getCourseId())/threadeddiscussions/\(contentItemId)/topics/\(tpicId)/response", "POST", ["Content-Type":"application/json"], body, false)
        if json != nil {
            var dict = json["response"] as! NSArray
            for item in dict {
        
            }
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
    
    func setContentId( ids: [String]) {
        contentItemIds = ids
        println(ids)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController is DiscussViewController {
            let vc = segue.destinationViewController as! DiscussViewController
            let idxPath : Int? = self.tblView.indexPathForSelectedRow()?.row
            let itemLst = contentItemIds!
            vc.setInputIds(course.getCourseId(),
                tpc: itemArray[idxPath!], did: itemLst[idxPath!])
            
            return
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        cell = self.tblView.dequeueReusableCellWithIdentifier("dtCell") as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "dtCell")
        }
        
        
        let tpic = itemArray[indexPath.row].title as String!
        cell?.textLabel?.font = UIFont(name: "Arial", size: 12.0)
        cell?.textLabel?.text = htmlToPlain(tpic)
        //let tpc: UILabel = cell?.viewWithTag(3) as UILabel
        //tpc.text = tpic
        //let desc:UIWebView = cell?.viewWithTag(2) as! UIWebView
        //desc.loadHTMLString(tpic, baseURL: nil)
        
        var img : UIImage = UIImage(named: "mb.png")!
        //let icon:UIImageView = cell?.viewWithTag(1) as! UIImageView
        //icon.image = img
        cell?.imageView?.image = img
        
        return cell!
    }
    
    
}
