//
//  Constants.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 8/13/14.
//  Copyright (c) 2014 Karvi Technologies, Inc. All rights reserved.
//

import Foundation
import UIKit
class GlobalConstants {
    
    func baseURL() ->String {
        return "https://api.learningstudio.com"
    }
    
    func grantType() ->String {
        return "password"
    }
    
    func clientId() ->String {
        return "affac082-457e-43aa-9a61-41dc1ca12a60"
//return "74a83663-3573-4dcd-b7bb-7631f4b3607a"
    }
    
    func userNameTeacher() ->String {
        return "raoyingteacher"
    }
    
    func userNameStudent() ->String {
        //return "hhstu2"//
        return "hhteach1"
        //return "raoyingstudent"
    }
    
    func passWord() ->String {
        return "letmein"
    }
    
    func clientString() ->String {
        return "gbtestc"
    }
    
    func schoolName() ->String {
        return "Regis University"
    }
    
    func passwordReset_Title()->String {
        return "Password Reset"
    }
    
    func passwordReset_Msg()->String {
        return "If you want to continue, please fill your email before clicking Continue, a email will be sent to you and you can start from there"
    }
    
    func textContinue()->String {
        return "Continue"
    }
    
    func textCancel()->String {
        return "Cancel"
    }
    
    func textEmailAddress()->String {
        return "Email address"
    }
    
    func textOk()->String {
        return "Ok"
    }
    
    func login_alertTitle()->String {
        return "Error"
    }
    
    func login_alertMsg()->String {
        return "Incorrect username or password, please try again"
    }
    
    //This is for customizing the button appearance
    func loginButton() ->(txtColor : UIColor, bkColor : UIColor, butImage : UIImage?) { //return txtColor, bkColor, image
        return (UIColor(red:1.0, green: 1.0, blue:1.0, alpha:1.0),
            UIColor(red:0, green: 0.81, blue:1.0, alpha:1.0),
            nil)
    }
    
    func forgotButton() ->(txtColor : UIColor, bkColor : UIColor, butImage : UIImage?) { //return txtColor, bkColor, image
        return (UIColor(red:1.0, green: 1.0, blue:1.0, alpha:1.0),
            UIColor(red:0, green: 0.81, blue:1.0, alpha:1.0),
            nil)
    }
    
    func courseView_navBackground()->UIColor {
        return UIColor(red:0, green: 0.81, blue:1.0, alpha:1.0)
    }
    
    func getImage_schoolLogo()->UIImage {
        return UIImage(named:"RU-logo-official_rev.png")!
    }
    
    func getSchoolLogo_filePath()->String {
        return "RU-logo-officail_rev.png"
    }
    
    /*
    func schoolIcon() ->UIImage {
        return new UIImage.load("test.jpg")
    }*/
    
}