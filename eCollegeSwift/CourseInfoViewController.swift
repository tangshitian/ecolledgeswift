//
//  CourseInfoViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 1/14/15.
//  Copyright (c) 2015 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class CourseInfoViewController: UITabBarController {
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    
    let gc = GlobalConstants()
    
    var course:Course! = nil
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    func setCourse(cur :Course) {
        self.course = cur
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController is InstructorsView {
            let vc = segue.destinationViewController as! InstructorsView
            vc.setCourse(course)
            return
        }
        if segue.destinationViewController is StudentsViewController {
            let vc = segue.destinationViewController as! StudentsViewController
            vc.setCourse(course)
            return
        }
        
        
    }
}
