//
//  TermsViewController.swift
//  eCollegeSwift
//
//  Created by Steven Tang on 9/21/14.
//  Copyright (c) 2014 Karvi Technologies, Inc. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var termsView: UITableView!
    let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
    
    let gc = GlobalConstants()
    var termArray = [Term]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.termsView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.termsView.dataSource = self
        
        var json = doMethodEx(appDel, "me/terms", "GET", nil, nil, false)
        if json != nil {
            var dict : NSArray = json["terms"] as! NSArray
            /*
            for item in dict {
                var term = Term(id: item["id"] as UInt,
                    name: item["name"] as String!,
                    desp: item["description"] as String!,
                    sDate: item["startDateTime"] as String!,
                    eDate: item["endDateTime"] as String!)
                termArray.append(term)
            }*/
        }
        
    }
        
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        var term : Term = termArray[indexPath.row]
        
        //TODO: launch courses view
        var alert: UIAlertView = UIAlertView()
        alert.title = term.getTerm().retName
        alert.message = term.getTerm().retDesp
        alert.addButtonWithTitle("Ok")
        alert.show() 
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return termArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.termsView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        cell.textLabel?.text = termArray[indexPath.row].getTerm().retName
        var courseImage : UIImage = UIImage(named: "term.png")! //TODO: replace with term png
        cell.imageView?.image = courseImage
        return cell
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var idx : Int! = termsView.indexPathForSelectedRow()?.row
        if idx != nil {
            var term  = termArray[idx]
            //segue.setValue(term, forKey: "TERMITEM")
            //segue.destinationViewController.
            let vc = segue.destinationViewController as! CoursesViewController
            //vc.setTerm(term)
        }
    }

}